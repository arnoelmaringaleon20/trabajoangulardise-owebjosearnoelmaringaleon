import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { WorkComponent } from './work/work.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './landing/landing.component';



@NgModule({
  declarations: [HeaderComponent, ProfileComponent, WorkComponent, FooterComponent, LandingComponent],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
